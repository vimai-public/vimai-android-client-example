package fi.vimai.client.example.ui

import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

import fi.vimai.client.example.R
import fi.vimai.client.example.databinding.OverviewFragmentBinding
import fi.vimai.navigation.location.LocationPermissionDeniedException
import fi.vimai.navigation.location.NoBuildingNearByException
import fi.vimai.navigation.minimap.MinimapFragment
import fi.vimai.navigation.server.entities.Area
import fi.vimai.navigation.server.entities.Building
import fi.vimai.navigation.service.ServiceStatus
import fi.vimai.navigation.service.VimaiService
import fi.vimai.navigation.service.VimaiServiceConnection
import fi.vimai.navigation.utils.showUserError
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import androidx.appcompat.app.AppCompatActivity



class OverviewFragment : Fragment() {

    companion object {
        const val MINIMAP_FRAGMENT_TAG = "minimap"
    }

    private var binding: OverviewFragmentBinding? = null

    private var building: Building? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val argBuilding = arguments?.getSerializable("building") as Building?

        if (binding == null) {  //first time run
            binding = DataBindingUtil.inflate(inflater, R.layout.overview_fragment, container, false)
            binding?.ivStartNavigation?.setOnClickListener {
                building?.let { building ->
                    val args = bundleOf("title" to building.name)
                    findNavController().navigate(R.id.action_overviewFragment_to_navigationFragment, args)
                }
            }
            binding?.metadataInitialized = false
            binding?.positioningInitialized = false
        }
        if (argBuilding != null) {
            building = argBuilding
        }
        connection.bind(context!!)
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private val connection = object: VimaiServiceConnection() {

        override fun onServiceAvailable(service: VimaiService) {
            initialize()
        }

        override fun onServiceUnavailable() {
        }
    }

    private fun initialize() {
        setup().observeOn(AndroidSchedulers.mainThread())
            .subscribe({ status ->
                when (status) {
                    ServiceStatus.METADATA_INITIALIZED -> {
                        if (isResumed) {
                            showMinimap(connection.service.currentAreas)
                        }
                        binding?.metadataInitialized = true
                    }
                }
        }, { t: Throwable ->
                when (t) {
                    is NoBuildingNearByException -> binding?.noNearbyBuildings = true
                    is LocationPermissionDeniedException -> binding?.noNearbyBuildings = true
                    else -> showUserError(context, t)
                }
                binding?.metadataInitialized = true
                binding?.positioningInitialized = true
        }, {
            binding?.positioningInitialized = true
            binding?.metadataInitialized = true
        }).also { connection.disposables.add(it) }
    }


    private fun setup(): Observable<ServiceStatus> {
        val buildingSingle = if (building != null) {
            Single.just(building)
        } else {
            connection.service.findClosestBuilding(activity!!).observeOn(AndroidSchedulers.mainThread()).map { building ->
                (activity as AppCompatActivity).supportActionBar?.title = building.name
                this.building = building
                building
            }
        }

        return buildingSingle.toObservable().flatMap { building ->
                connection.service.setupPositioningAndNavigation(building.buildingID)
            }
    }

    override fun onResume() {
        super.onResume()
        if (connection.bound && connection.service.currentAreas.isNotEmpty()) {
            showMinimap(connection.service.currentAreas)
        }
    }

    private fun showMinimap(areas: List<Area>) {
        val minimapFragment = childFragmentManager.findFragmentByTag(MINIMAP_FRAGMENT_TAG) ?: MinimapFragment.newInstance(areas)
        if (!minimapFragment.isAdded || minimapFragment.isRemoving) {
            childFragmentManager.beginTransaction()
                .replace(R.id.minimapPreview, minimapFragment,
                    MINIMAP_FRAGMENT_TAG
                ).commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.overview, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_buildings_list -> findNavController().navigate(R.id.buildingsFragment)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        connection.unbind()
        super.onDestroyView()
    }

}
